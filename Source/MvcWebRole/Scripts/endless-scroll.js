var wallBaseUrl = null;
var wallLoading = false;
var lastScrollPosition = 0;

function initWall(url) {
    wallBaseUrl = url;
    enableEndlessScrolling();
}

function enableEndlessScrolling() {
    $(window).scroll(function () {
        if (wallLoading) {
            return;
        }

        var scrollPosition = $(window).scrollTop();
        if (scrollPosition == lastScrollPosition) {
            return;
        }
        lastScrollPosition = scrollPosition;

        if (scrollPosition == ($(document).height() - $(window).height())) {
            endlessScrollingCallback();
        }
    });
}

function endlessScrollingCallback(fireSequence, pageSequence, scrollDirection) {
    $("#bottomLoading").show();

    $.get(wallBaseUrl + "?startFrom=" + window.wallLastPostId, function (html) {
        if ($.trim(html).length > 0) {
            $("#wallContainer").append(html);
        } else {
            $("#noMorePosts").show();
        }
    }).always(function () {
        $("#bottomLoading").hide();
        wallLoading = false;
    });
}
