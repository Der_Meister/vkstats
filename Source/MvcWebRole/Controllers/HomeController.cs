﻿using MvcWebRole.Models;
using System.Web.Mvc;
using VKStats.Domain.Repository;

namespace MvcWebRole.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IRepositoryFactory repositoryFactory;

        public HomeController(IRepositoryFactory repositoryFactory)
        {
            this.repositoryFactory = repositoryFactory;
        }

        public ActionResult Index()
        {
            var wallRepository = repositoryFactory.GetWallRepository(0);
            var startFrom = wallRepository.GetTopId(0);

            return View(new WallViewModel { WallOwnerId = 0, StartFrom = startFrom, Count = WallController.DefaultRecordsCount });
        }

        public ActionResult WallPartial(int startFrom)
        {
            return View("_WallPartial", new WallViewModel { WallOwnerId = 0, StartFrom = startFrom - 1, Count = WallController.DefaultRecordsCount });
        }
    }
}