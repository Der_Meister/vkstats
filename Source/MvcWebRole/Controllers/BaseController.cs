﻿using System;
using System.Security.Claims;
using System.Web.Mvc;
using NLog;

namespace MvcWebRole.Controllers
{
    public class BaseController : Controller
    {
        protected static Logger Logger = LogManager.GetCurrentClassLogger();

        protected int UserId { get; private set; }

        protected override void OnException(ExceptionContext filterContext)
        {
            Logger.Error(filterContext.Exception);

            filterContext.Result = View("~/Views/Shared/Error.cshtml");
            filterContext.ExceptionHandled = true;

            base.OnException(filterContext);
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            ClaimsIdentity claimsIdentity = User.Identity as ClaimsIdentity;

            if (claimsIdentity != null && claimsIdentity.IsAuthenticated)
            {
                // Access claims
                foreach (Claim claim in claimsIdentity.Claims)
                {
                    if (claim.Type == "urn:vkontakte:name")
                    {
                        ViewBag.UserName = claim.Value;
                    }
                    else if (claim.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier")
                    {
                        UserId = Convert.ToInt32(claim.Value);
                    }
                }
            }
        }
    }
}