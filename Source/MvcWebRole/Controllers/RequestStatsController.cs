﻿using System.Web.Mvc;
using MvcWebRole.Models;
using VKStats.Queue;

namespace MvcWebRole.Controllers
{
    public class RequestStatsController : BaseController
    {
        private readonly IRequestStatsQueue requestStatsQueue;

        public RequestStatsController(IRequestStatsQueue requestStatsQueue)
        {
            this.requestStatsQueue = requestStatsQueue;
        }

        // GET: RequestStats
        public ActionResult Index()
        {
            return View(new RequestStatsViewModel());
        }

        [HttpPost]
        public ActionResult Index(RequestStatsViewModel model)
        {
            if (!ModelState.IsValid)
                return View();

            requestStatsQueue.AddItem(new RequestStatsData { WallOwnerId = UserId, VKUserScreenName = model.VKUserScreenName });
            return View("RequestStatsSuccess", new RequestStatsResultViewModel { Private = UserId != 0 });
        }
    }
}