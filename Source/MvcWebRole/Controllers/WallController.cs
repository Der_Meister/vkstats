﻿using System.Linq;
using System.Web.Mvc;
using MvcWebRole.Models;
using VKStats.Domain.Repository;

namespace MvcWebRole.Controllers
{
    [Authorize]
    public class WallController : BaseController
    {
        public const int DefaultRecordsCount = 10;
        private readonly IRepositoryFactory repositoryFactory;

        public WallController(IRepositoryFactory repositoryFactory)
        {
            this.repositoryFactory = repositoryFactory;
        }

        public ActionResult Index()
        {
            var wallRepository = repositoryFactory.GetWallRepository(UserId);
            var startFrom = wallRepository.GetTopId(UserId);

            return View(new WallViewModel { WallOwnerId = UserId, StartFrom = startFrom, Count = DefaultRecordsCount });
        }

        [ChildActionOnly, AllowAnonymous]
        [OutputCache(Duration = 600, VaryByParam = "*")]
        public ActionResult WallList(int wallOwnerId, int startFrom, int count)
        {
            var wallRepository = repositoryFactory.GetWallRepository(0);

            var records = wallRepository.GetTopRecords(wallOwnerId, startFrom, count).ToList();
            var last = records.LastOrDefault();

            var model = new WallListViewModel { Records = records, LastPostId = last != null ? last.Id : 0 };

            return View(model);
        }

        public ActionResult WallPartial(int startFrom)
        {
            return View("_WallPartial", new WallViewModel { WallOwnerId = UserId, StartFrom = startFrom - 1, Count = WallController.DefaultRecordsCount });
        }
    }
}