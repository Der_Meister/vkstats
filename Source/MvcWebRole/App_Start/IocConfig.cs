﻿using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Microsoft.Azure;
using StackExchange.Redis.Extensions.Core;
using StackExchange.Redis.Extensions.Jil;
using VKStats.Queue;
using VKStats.CacheLayer;

namespace MvcWebRole
{
    public class IocConfig
    {
        public static void Register()
        {
            var builder = new ContainerBuilder();

            // Register your MVC controllers.
            builder.RegisterControllers(typeof (MvcApplication).Assembly);

            builder.RegisterType<RequestStatsQueue>().As<IRequestStatsQueue>();
            builder.RegisterType<HybridRepositoryFactory>().AsImplementedInterfaces();

            builder.RegisterType<JilSerializer>().As<ISerializer>();
            builder.Register(context => new StackExchangeRedisCacheClient(context.Resolve<ISerializer>(), CloudConfigurationManager.GetSetting("RedisConnectionString"))).As<ICacheClient>();

            // Set the dependency resolver to be Autofac.
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}