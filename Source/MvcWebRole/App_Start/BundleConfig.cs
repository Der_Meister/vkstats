﻿using System.Web.Optimization;

namespace MvcWebRole
{
    public class BundleConfig
    {
        private static string version = typeof(BundleConfig).Assembly.GetName().Version.ToString();

        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.UseCdn = true;

            bundles.Add(CreateScriptBundle("bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(CreateScriptBundle("bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(CreateScriptBundle("bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(CreateScriptBundle("bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(CreateScriptBundle("bundles/site").Include("~/Scripts/endless-scroll.js"));

            bundles.Add(CreateStyleBundle("Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/font-awesome.css",
                      "~/Content/site.css"));
        }

        private static ScriptBundle CreateScriptBundle(string pathPart)
        {
            return new ScriptBundle(GetVirtualPath(pathPart), GetCdnPath(pathPart));
        }

        private static StyleBundle CreateStyleBundle(string pathPart)
        {
            return new StyleBundle(GetVirtualPath(pathPart), GetCdnPath(pathPart));
        }

        private static string GetVirtualPath(string pathPart)
        {
            return string.Format("~/{0}", pathPart);
        }

        private static string GetCdnPath(string pathPart)
        {
            return string.Format("http://az792522.vo.msecnd.net/{0}?v={1}", pathPart, version);
        }
    }
}
