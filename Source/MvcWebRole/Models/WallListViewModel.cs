﻿using System.Collections.Generic;
using VKStats.Domain.Entity;

namespace MvcWebRole.Models
{
    public class WallListViewModel
    {
        public List<WallRecord> Records { get; set; }
        public int LastPostId { get; set; }
    }
}