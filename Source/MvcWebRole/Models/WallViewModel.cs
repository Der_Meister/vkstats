﻿namespace MvcWebRole.Models
{
    public class WallViewModel
    {
        public int WallOwnerId { get; set; }
        public int StartFrom { get; set; }
        public int Count { get; set; }
    }
}