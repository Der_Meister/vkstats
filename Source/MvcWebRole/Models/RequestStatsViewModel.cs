﻿using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace MvcWebRole.Models
{
    public class RequestStatsViewModel
    {
        private const string ScreenNameRegex = @"\Ahttps?://vk.com/([A-z0-9\._]+).*\Z";

        private readonly Regex screenNameRegex = new Regex(ScreenNameRegex);

        [Required]
        [Display(Name = "VK user page")]
        [RegularExpression(ScreenNameRegex, ErrorMessage = "Enter a correct URL. Example: https://vk.com/durov")]
        public string VKUserPageUrl { get; set; }

        public string VKUserScreenName
        {
            get
            {
                if (screenNameRegex.IsMatch(VKUserPageUrl))
                    return screenNameRegex.Match(VKUserPageUrl).Groups[1].Value;

                return null;
            }
        }
    }
}