﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VKStats.Domain.Entity
{
    public enum WallRecordType : byte
    {
        Default,
        Warning,
        Error,
    }

    /// <summary>
    /// Result info with graph and URL.
    /// </summary>
    public class WallRecord
    {
        [Key]
        public int Id { get; set; }
        [Index]
        public int WallOwnerId { get; set; }
        [Index]
        public DateTime CreatedAt { get; set; }

        [MaxLength(200)]
        public string PersonName { get; set; }
        [MaxLength(100)]
        public string AvatarUrl { get; set; }
        [MaxLength(100)]
        public string MostPopularPostUrl { get; set; }
        [MaxLength(150)]
        public string ChartUrl { get; set; }

        public WallRecordType Type { get; set; }
        [MaxLength(200)]
        public string Message { get; set; }
    }
}