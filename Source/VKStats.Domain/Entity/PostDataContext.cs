﻿using System.Data.Entity;

namespace VKStats.Domain.Entity
{
    public class PostDataContext : DbContext
    {
        public PostDataContext()
        {
        }

        public PostDataContext(string connectionString) : base(connectionString)
        {
        }

        public DbSet<Post> Posts { get; set; }
    }
}