﻿using System.Data.Entity;

namespace VKStats.Domain.Entity
{
    public class WallDataContext : DbContext
    {
        public WallDataContext()
        {
        }

        public WallDataContext(string connectionString) : base(connectionString)
        {
        }

        public DbSet<WallRecord> WallRecords { get; set; }
    }
}