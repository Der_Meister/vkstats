﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VKStats.Domain.Entity
{
    /// <summary>
    /// Information about VK post.
    /// </summary>
    public class Post
    {
        [Key, Column(Order = 0)]
        public int UserId { get; set; }

        [Key, Column(Order = 1)]
        public int PostId { get; set; }

        public int Length { get; set; }
        public int Likes { get; set; }
    }
}