﻿using System.Collections.Generic;
using VKStats.Domain.Entity;

namespace VKStats.Domain.Repository
{
    public interface IWallRepository
    {
        int AddRecord(WallRecord record);
        IEnumerable<WallRecord> GetTopRecords(int wallOwnerId, int startFrom, int count);
        int GetTopId(int wallOwnerId);
    }
}