﻿using System.Collections.Generic;
using VKStats.Domain.Entity;

namespace VKStats.Domain.Repository
{
    public interface IPostRepository
    {
        void AddOrUpdatePost(Post post);
        void AddPosts(ICollection<Post> posts);
        IEnumerable<Post> GetUserPosts(int userId);
    }
}