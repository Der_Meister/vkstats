﻿namespace VKStats.Domain.Repository
{
    public interface IRepositoryFactory
    {
        /// <summary>
        /// Constructs post repository using userId as shard key.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        IPostRepository GetPostRepository(int userId);

        /// <summary>
        /// Constructs wall repository using wallOwnerId as shard key.
        /// </summary>
        /// <param name="wallOwnerId"></param>
        /// <returns></returns>
        IWallRepository GetWallRepository(int wallOwnerId);
    }
}
