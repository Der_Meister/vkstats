﻿using StackExchange.Redis.Extensions.Core;
using VKStats.Domain.Repository;
using VKStats.SqlRepository;

namespace VKStats.CacheLayer
{
    public class HybridRepositoryFactory : IRepositoryFactory
    {
        private readonly ICacheClient cacheClient;
        private readonly SqlRepositoryFactory sqlRepositoryFactory;

        public HybridRepositoryFactory(ICacheClient cacheClient)
        {
            this.cacheClient = cacheClient;
            sqlRepositoryFactory = new SqlRepositoryFactory();
        }

        public IPostRepository GetPostRepository(int userId)
        {
            return sqlRepositoryFactory.GetPostRepository(userId);
        }

        public IWallRepository GetWallRepository(int wallOwnerId)
        {
            return new WallRepositoryCacheDecorator(sqlRepositoryFactory.GetWallRepository(wallOwnerId), cacheClient);
        }
    }
}