﻿using System;
using System.Collections.Generic;
using System.Linq;
using StackExchange.Redis;
using StackExchange.Redis.Extensions.Core;
using VKStats.Domain.Entity;
using VKStats.Domain.Repository;

namespace VKStats.CacheLayer
{
    public class WallRepositoryCacheDecorator : IWallRepository
    {
        private const string WallTopPrefix = "Wall_TopId_";
        private readonly ICacheClient cacheClient;
        private readonly IWallRepository decorated;
        private readonly TimeSpan smallTimeout = TimeSpan.FromMinutes(1);
        private readonly TimeSpan largeTimeout = TimeSpan.FromMinutes(10);
        private readonly string keyPrefix;

        public WallRepositoryCacheDecorator(IWallRepository decorated, ICacheClient cacheClient)
        {
            this.decorated = decorated;
            this.cacheClient = cacheClient;

            keyPrefix = "V" + typeof (WallRepositoryCacheDecorator).Assembly.GetName().Version.ToString() + "_";
        }

        public int GetTopId(int wallOwnerId)
        {
            var key = keyPrefix + WallTopPrefix + wallOwnerId;

            if (cacheClient.Exists(key))
            {
               return Convert.ToInt32(cacheClient.Get<string>(key));
            }

            var id = decorated.GetTopId(wallOwnerId);
            cacheClient.Add(key, id.ToString(), smallTimeout);

            return id;
        }

        public IEnumerable<WallRecord> GetTopRecords(int wallOwnerId, int startFrom, int count)
        {
            var key = keyPrefix + "WallTopRecords_" + wallOwnerId + "_" + startFrom + "_" + count;

            if (cacheClient.Exists(key))
            {
                cacheClient.Database.KeyExpire(key, largeTimeout, CommandFlags.FireAndForget); // Sliding interval.
                return cacheClient.Get<List<WallRecord>>(key);
            }

            var records = decorated.GetTopRecords(wallOwnerId, startFrom, count).ToList();
            cacheClient.Add(key, records, largeTimeout);

            return records;
        }

        public int AddRecord(WallRecord record)
        {
            RedisKey key = keyPrefix + WallTopPrefix + record.WallOwnerId;
            var id = decorated.AddRecord(record);

            cacheClient.Add(key, id.ToString(), smallTimeout);

            return id;
        }
    }
}