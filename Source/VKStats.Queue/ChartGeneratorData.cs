﻿namespace VKStats.Queue
{
    public class ChartGeneratorData
    {
        public UserInfoDto User { get; set; }
        public string MostPopularPostUrl { get; set; }

        /// <summary>
        /// 0 - public post, other values - post on a private wall.
        /// </summary>
        public int WallOwnerId { get; set; }
    }
}