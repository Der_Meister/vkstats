﻿using System;
using Microsoft.Azure;

namespace VKStats.Queue
{
    public interface IRequestStatsQueue
    {
        void AddItem(RequestStatsData item);
        void BeginRead(Func<RequestStatsData, Action, bool> callback);
        void EndRead();
    }

    public class RequestStatsQueue : BaseQueue<RequestStatsData>, IRequestStatsQueue
    {
        public RequestStatsQueue() : base(CloudConfigurationManager.GetSetting("VKStats.ServiceBus.ConnectionString"), "RequestStatsQueue")
        {
        }
    }
}