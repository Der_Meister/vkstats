﻿using Microsoft.Azure;
using System;

namespace VKStats.Queue
{
    public interface IChartGeneratorQueue
    {
        void AddItem(ChartGeneratorData item);
        void BeginRead(Func<ChartGeneratorData, Action, bool> callback);
        void EndRead();
    }

    public class ChartGeneratorQueue : BaseQueue<ChartGeneratorData>, IChartGeneratorQueue
    {
        public ChartGeneratorQueue() : base(CloudConfigurationManager.GetSetting("VKStats.ServiceBus.ConnectionString"), "ChartGeneratorQueue")
        {
        }
    }
}