﻿namespace VKStats.Queue
{
    public class RequestStatsData
    {
        public int WallOwnerId { get; set; }
        public string VKUserScreenName { get; set; }
    }
}