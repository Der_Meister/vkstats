﻿using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using NLog;
using System;
using System.Globalization;

namespace VKStats.Queue
{
    public class BaseQueue<T> where T : class
    {
        private string connectionString;
        private QueueClient queueClient;

        protected static Logger Logger = LogManager.GetCurrentClassLogger();

        public BaseQueue(string connectionString, string queueName)
        {
            this.connectionString = connectionString;
            QueueName = queueName;
        }

        protected string QueueName { get; }

        protected QueueClient GetQueueClient()
        {
            var namespaceManager = NamespaceManager.CreateFromConnectionString(connectionString);
            if (!namespaceManager.QueueExists(QueueName))
            {
                namespaceManager.CreateQueue(QueueName);
            }

            return QueueClient.CreateFromConnectionString(connectionString, QueueName);
        }

        public void AddItem(T item)
        {
            QueueClient client = null;

            try
            {
                client = GetQueueClient();

                client.Send(new BrokeredMessage(item));
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            finally
            {
                if (client != null)
                    client.Close();
            }
        }

        public void BeginRead(Func<T, Action, bool> callback)
        {
            if (callback == null)
                throw new NullReferenceException("Callback can't be null.");

            if (queueClient == null)
                queueClient = GetQueueClient();

            queueClient.OnMessage(receivedMessage =>
            {
                try
                {
                    var data = receivedMessage.GetBody<T>();

                    if (callback(data, () => receivedMessage.RenewLock()))
                    {
                        receivedMessage.Complete();
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(ex);
                }
            }, new OnMessageOptions { AutoComplete = false, MaxConcurrentCalls = 1 });
        }

        public void EndRead()
        {
            if (queueClient != null)
            {
                queueClient.Close();
            }
        }
    }
}