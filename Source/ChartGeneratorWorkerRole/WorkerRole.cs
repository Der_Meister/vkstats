﻿using System;
using System.Net;
using System.Threading;
using Autofac;
using Microsoft.WindowsAzure.ServiceRuntime;
using NLog;
using VKStats.Queue;
using VKStats.Domain.Repository;
using VKStats.ChartGeneration;
using VKStats.Domain.Entity;
using StackExchange.Redis.Extensions.Core;
using Microsoft.Azure;
using StackExchange.Redis.Extensions.Jil;
using VKStats.CacheLayer;

namespace ChartGeneratorWorkerRole
{
    public class WorkerRole : RoleEntryPoint
    {
        private readonly ManualResetEvent completedEvent = new ManualResetEvent(false);
        // QueueClient is thread-safe. Recommended that you cache 
        // rather than recreating it on every request
        private IChartGeneratorQueue chartGeneratorQueue;
        private IContainer container;
        protected Logger Logger = LogManager.GetCurrentClassLogger();

        public override void Run()
        {
            try
            {
                Logger.Info("Starting processing of messages");

                // Initiates the message pump and callback is invoked for each message that is received, calling close on the client will stop the pump.
                chartGeneratorQueue = container.Resolve<IChartGeneratorQueue>();
                var repositoryFactory = container.Resolve<IRepositoryFactory>();
                var imageStorage = container.Resolve<IImageStorage>();
                var chartGenerator = container.Resolve<ChartImageGenerator>();

                chartGeneratorQueue.BeginRead((data, renewLock) =>
                {
                    try
                    {
                        // Process the message
                        var postRepository = repositoryFactory.GetPostRepository(data.User.Id);
                        var chartUrl = chartGenerator.Generate(data.User.Id);
                        renewLock();

                        var wallRepository = repositoryFactory.GetWallRepository(data.WallOwnerId);
                        wallRepository.AddRecord(new WallRecord
                            {
                                WallOwnerId = data.WallOwnerId,
                                CreatedAt = DateTime.UtcNow,
                                PersonName = string.Format("{0} {1}", data.User.FirstName, data.User.LastName),
                                AvatarUrl = data.User.AvatarUrl,
                                ChartUrl = chartUrl,
                                MostPopularPostUrl = data.MostPopularPostUrl,
                                Type = WallRecordType.Default,
                            });

                        Logger.Info("Generated chart for user: {0}", data.User.Id);
                        return true;
                    }
                    catch (Exception ex)
                    {
                        // Handle any message processing specific exceptions here
                        Logger.Error(ex);
                    }
                    return false;
                });

                completedEvent.WaitOne();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }
        }

        public override bool OnStart()
        {
            try
            {
                ConfigureIoc();

                // Set the maximum number of concurrent connections 
                ServicePointManager.DefaultConnectionLimit = 12;

                return base.OnStart();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }
        }

        public override void OnStop()
        {
            try
            {
                // Close the connection to Service Bus Queue
                if (chartGeneratorQueue != null)
                    chartGeneratorQueue.EndRead();

                completedEvent.Set();
                base.OnStop();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }
        }

        private void ConfigureIoc()
        {
            // Autofac configuration
            var builder = new ContainerBuilder();

            // Register your dependencies
            builder.RegisterType<ChartGeneratorQueue>().AsImplementedInterfaces();
            builder.RegisterType<HybridRepositoryFactory>().AsImplementedInterfaces();
            builder.RegisterType<BlobStorage>().AsImplementedInterfaces();
            builder.RegisterType<ChartImageGenerator>().AsSelf();

            builder.RegisterType<JilSerializer>().As<ISerializer>();
            builder.Register(context => new StackExchangeRedisCacheClient(context.Resolve<ISerializer>(), CloudConfigurationManager.GetSetting("RedisConnectionString"))).As<ICacheClient>();

            container = builder.Build();
        }
    }
}