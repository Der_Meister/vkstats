﻿using System;
using System.Net;
using System.Threading;
using Autofac;
using Microsoft.WindowsAzure.ServiceRuntime;
using NLog;
using VKStats.Queue;
using VKStats.ApiIntegration;
using VKStats.Domain.Repository;
using PostEntity = VKStats.Domain.Entity.Post;
using VKStats.Domain.Entity;
using VKStats.CacheLayer;
using StackExchange.Redis.Extensions.Jil;
using StackExchange.Redis.Extensions.Core;
using Microsoft.Azure;

namespace VKDownloaderWorkerRole
{
    public class WorkerRole : RoleEntryPoint
    {
        private readonly ManualResetEvent completedEvent = new ManualResetEvent(false);
        private IContainer container;
        protected Logger Logger = LogManager.GetCurrentClassLogger();
        // QueueClient is thread-safe. Recommended that you cache 
        // rather than recreating it on every request
        private IRequestStatsQueue requestStatsQueue;
        private UserReader userReader = new UserReader();
        private WallReader wallReader = new WallReader(WallReader.StandardRequestsPerSecond);

        public override void Run()
        {
            try
            {
                Logger.Info("Starting processing of messages");

                // Initiates the message pump and callback is invoked for each message that is received, calling close on the client will stop the pump.
                requestStatsQueue = container.Resolve<IRequestStatsQueue>();
                var chartGeneratorQueue = container.Resolve<IChartGeneratorQueue>();
                var repositoryFactory = container.Resolve<IRepositoryFactory>();

                requestStatsQueue.BeginRead((data, renewLock) =>
                {
                    string pageUrl = null;
                    try
                    {
                        // Process the message
                        UserInfo userInfo;
                        pageUrl = string.Format("https://vk.com/{0}", data.VKUserScreenName);

                        try
                        {
                            userInfo = userReader.GetUserInfo(data.VKUserScreenName);
                        }
                        catch (UserNotFoundException ex2)
                        {
                            Logger.Warn(ex2, "Wrong user id passed to queue.");
                            AddMessage(data.WallOwnerId, WallRecordType.Error, "Page does not exist: " + pageUrl);
                            return true;
                        }
                        renewLock();

                        var maxLikes = -1;
                        var mostPopularPostId = 0;

                        var repository = repositoryFactory.GetPostRepository(userInfo.Id);
                        wallReader.ProcessAllPosts(userInfo.Id, post =>
                        {
                            var text = post.Text ?? "";

                            // Length in characters.
                            var length = text.Length;

                            if (post.Likes > maxLikes)
                            {
                                maxLikes = post.Likes;
                                mostPopularPostId = post.PostId;
                            }

                            Logger.Trace("Saving post info: ({0}, {1})", post.UserId, post.PostId);
                            repository.AddOrUpdatePost(new PostEntity { UserId = post.UserId, PostId = post.PostId, Likes = post.Likes, Length = length });
                            renewLock();
                        });

                        // Request chart generation.
                        var userInfoDto = new UserInfoDto { Id = userInfo.Id, FirstName = userInfo.FirstName, LastName = userInfo.LastName, AvatarUrl = userInfo.AvatarUrl };

                        if (maxLikes >= 0)
                        {
                            chartGeneratorQueue.AddItem(new ChartGeneratorData { User = userInfoDto, MostPopularPostUrl = UrlHelper.GetPostLink(userInfo.Id, mostPopularPostId), WallOwnerId = data.WallOwnerId });
                        }
                        else
                        {
                            Logger.Info("No posts found for user: {0}", userInfo.Id);
                            AddMessage(data.WallOwnerId, WallRecordType.Warning, "Page does not have posts: " + pageUrl);
                        }

                        Logger.Info("Processed request for user: {0} ({1})", data.VKUserScreenName, userInfo.Id);
                        return true;
                    }
                    catch (WallHiddenException ex)
                    {
                        // TODO: Notify user about result.
                        Logger.Warn(ex, "Wall is hidden.");
                        try
                        {
                            AddMessage(data.WallOwnerId, WallRecordType.Warning, "Only logged in users can see this profile: " + pageUrl);
                            return true;
                        }
                        catch (Exception ex2)
                        {
                            Logger.Error(ex2);
                        }
                    }
                    catch (Exception ex)
                    {
                        // Handle any message processing specific exceptions here
                        Logger.Error(ex);
                    }
                    return false;
                });

                completedEvent.WaitOne();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }
        }

        public override bool OnStart()
        {
            try
            {
                ConfigureIoc();

                // Set the maximum number of concurrent connections 
                ServicePointManager.DefaultConnectionLimit = 12;

                return base.OnStart();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }
        }

        public override void OnStop()
        {
            try
            {
                // Close the connection to Service Bus Queue
                if (requestStatsQueue != null)
                    requestStatsQueue.EndRead();

                completedEvent.Set();
                base.OnStop();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                throw;
            }
        }

        private void ConfigureIoc()
        {
            // Autofac configuration
            var builder = new ContainerBuilder();

            // Register your dependencies
            builder.RegisterType<RequestStatsQueue>().AsImplementedInterfaces();
            builder.RegisterType<ChartGeneratorQueue>().AsImplementedInterfaces();
            builder.RegisterType<HybridRepositoryFactory>().AsImplementedInterfaces();

            builder.RegisterType<JilSerializer>().As<ISerializer>();
            builder.Register(context => new StackExchangeRedisCacheClient(context.Resolve<ISerializer>(), CloudConfigurationManager.GetSetting("RedisConnectionString"))).As<ICacheClient>();

            container = builder.Build();
        }

        private void AddMessage(int wallOwnerId, WallRecordType type, string message)
        {
            var repositoryFactory = container.Resolve<IRepositoryFactory>();
            var wallRepository = repositoryFactory.GetWallRepository(wallOwnerId);
            wallRepository.AddRecord(new WallRecord
            {
                WallOwnerId = wallOwnerId,
                CreatedAt = DateTime.UtcNow,
                Type = type,
                Message = message,
            });
        }
    }
}