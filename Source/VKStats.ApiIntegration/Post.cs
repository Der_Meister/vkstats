﻿namespace VKStats.ApiIntegration
{
    public class Post
    {
        public int UserId { get; set; }
        public int PostId { get; set; }
        public string Text { get; set; }
        public int Likes { get; set; }
    }
}