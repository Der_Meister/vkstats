﻿using System;

namespace VKStats.ApiIntegration
{
    public class VKApiException : Exception
    {
        public VKApiException(int code, string message) : base("VK error: " + message)
        {
            Code = code;
        }

        public int Code { get; set; }
    }
}
