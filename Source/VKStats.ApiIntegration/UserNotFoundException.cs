﻿using System;

namespace VKStats.ApiIntegration
{
    public class UserNotFoundException : Exception
    {
        public UserNotFoundException(string userId) : base("Invalid user id: " + userId)
        {
        }
    }
}