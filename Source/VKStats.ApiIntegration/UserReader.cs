﻿using System.Net;
using System.Text;
using Newtonsoft.Json;

namespace VKStats.ApiIntegration
{
    public class UserReader
    {
        public UserInfo GetUserInfo(string domain)
        {
            var url = string.Format("https://api.vk.com/method/users.get?user_ids={0}&fields=photo_50&v=5.35", domain);
            using (var client = new WebClient())
            {
                client.Encoding = Encoding.UTF8;

                dynamic result = JsonConvert.DeserializeObject(client.DownloadString(url));

                if (result.response == null && result.error != null && result.error.error_code == 113)
                    throw new UserNotFoundException(domain);

                dynamic user = result.response[0];

                return new UserInfo { Id = user.id, FirstName = user.first_name, LastName = user.last_name, AvatarUrl = user.photo_50 };
            }
        }
    }
}