﻿namespace VKStats.ApiIntegration
{
    public class WallHiddenException : VKApiException
    {
        public WallHiddenException(int code, string message) : base(code, message)
        {
        }
    }
}
