﻿namespace VKStats.ApiIntegration
{
    public static class UrlHelper
    {
        public static string GetPostLink(int userId, int postId)
        {
            return string.Format("https://vk.com/wall{0}_{1}", userId, postId);
        }
    }
}