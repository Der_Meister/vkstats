﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading;
using Newtonsoft.Json;

namespace VKStats.ApiIntegration
{
    public class WallReader
    {
        private const int PostsPerRequest = 100;
        private const int MaximumPosts = 1000;
        private readonly int requestsPerSecond;

        public const int StandardRequestsPerSecond = 5;

        public WallReader(int requestsPerSecond)
        {
            this.requestsPerSecond = requestsPerSecond;
        }

        public void ProcessAllPosts(int userId, Action<Post> callback)
        {
            if (userId <= 0)
                throw new ArgumentOutOfRangeException("userId");

            if (callback == null)
                throw new ArgumentNullException("callback");

            var offset = 0;
            int totalPosts;
            var requests = 0;

            var startTime = DateTime.MinValue;

            do
            {
                if (requests == requestsPerSecond)
                {
                    requests = 0;
                    var sleepTime = (startTime.AddSeconds(1) - DateTime.Now).Milliseconds;
                    if (sleepTime > 0)
                        Thread.Sleep(sleepTime);
                }

                if (requests == 0)
                {
                    startTime = DateTime.Now;
                }

                var posts = ExecuteRequest(userId, offset, out totalPosts);
                requests++;
                offset += PostsPerRequest;

                foreach (var post in posts)
                {
                    callback(post);
                }
            } while (offset + PostsPerRequest < Math.Min(totalPosts, MaximumPosts));
        }

        private List<Post> ExecuteRequest(int userId, int offset, out int totalPosts)
        {
            var posts = new List<Post>();

            var url = string.Format("https://api.vk.com/method/wall.get?owner_id={0}&offset={1}&count={2}&filter=owner&extended=0&v=5.35", userId, offset, PostsPerRequest);
            using (var client = new WebClient())
            {
                client.Encoding = Encoding.UTF8;
                dynamic result = JsonConvert.DeserializeObject(client.DownloadString(url));

                if (result.response == null && result.error != null)
                {
                    int errorCode = result.error.error_code;
                    string errorMessage = result.error.error_msg;

                    if (errorCode == 15)
                        throw new WallHiddenException(errorCode, errorMessage);
                    else
                        throw new VKApiException(errorCode, errorMessage);
                }

                totalPosts = result.response.count;

                foreach (dynamic post in result.response.items)
                {
                    if (post.post_type == "copy") // Skip reposts.
                        continue;

                    posts.Add(new Post { UserId = post.from_id, PostId = post.id, Text = post.text, Likes = post.likes.count });
                }
            }

            return posts;
        }
    }
}