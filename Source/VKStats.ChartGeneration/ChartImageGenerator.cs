﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Windows.Forms.DataVisualization.Charting;
using VKStats.Domain.Repository;

namespace VKStats.ChartGeneration
{
    public class ChartImageGenerator
    {
        private readonly IRepositoryFactory repositoryFactory;
        private readonly IImageStorage imageStorage;

        public ChartImageGenerator(IRepositoryFactory repositoryFactory, IImageStorage imageStorage)
        {
            this.repositoryFactory = repositoryFactory;
            this.imageStorage = imageStorage;
        }

        public string Generate(int userId)
        {
            var postRepository = repositoryFactory.GetPostRepository(userId);
            var posts = postRepository.GetUserPosts(userId);

            var xValues = posts.Select(p => p.Length).ToList();
            var yValues = posts.Select(p => p.Likes).ToList();

            var series = new Series();
            series.ChartType = SeriesChartType.Point;
            series.Points.DataBindXY(xValues, yValues);

            var area = new ChartArea();
            area.AxisX.Minimum = 0d;
            area.AxisX.Maximum = Math.Max(Math.Ceiling(xValues.Max() / 100.0), 1) * 100;
            area.AxisX.Title = "Length, characters";

            area.AxisY.Minimum = 0d;
            area.AxisY.Maximum = Math.Max(Math.Ceiling(yValues.Max() / 100.0), 1) * 100;
            area.AxisY.Title = "Likes";

            var chart = new Chart();
            chart.Size = new Size(630, 450);
            chart.ChartAreas.Add(area);
            chart.Series.Add(series);

            using (var stream = new MemoryStream())
            {
                chart.SaveImage(stream, ImageFormat.Png);

                return imageStorage.Save(userId, stream);
            }
        }
    }
}