﻿using System;
using System.IO;
using Microsoft.WindowsAzure.Storage;
using Microsoft.Azure;

namespace VKStats.ChartGeneration
{
    public class BlobStorage : IImageStorage
    {
        private const string ContainerName = "charts";

        int GetPartition(int userId)
        {
            return userId % 0xff;
        }

        public string Save(int userId, Stream dataStream)
        {
            var blobName = string.Format("{0:x2}/{1}/{2}.png", GetPartition(userId), userId, Guid.NewGuid());

            // Retrieve storage account from connection string.
            var storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));

            // Create the blob client.
            var blobClient = storageAccount.CreateCloudBlobClient();

            // Retrieve reference to a previously created container.
            var container = blobClient.GetContainerReference(ContainerName);

            // Retrieve reference to a blob.
            var blockBlob = container.GetBlockBlobReference(blobName);

            dataStream.Seek(0, SeekOrigin.Begin);
            blockBlob.UploadFromStream(dataStream);

            blockBlob.Properties.ContentType = "image/png";
            blockBlob.SetProperties();

            return ContainerName + "/" + blobName;
        }
    }
}