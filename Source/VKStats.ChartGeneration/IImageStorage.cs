﻿using System.IO;

namespace VKStats.ChartGeneration
{
    public interface IImageStorage
    {
        string Save(int userId, Stream dataStream);
    }
}