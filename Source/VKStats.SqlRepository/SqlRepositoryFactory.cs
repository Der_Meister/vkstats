﻿using Microsoft.Azure;
using System;
using System.Collections.Generic;
using VKStats.Domain.Entity;
using VKStats.Domain.Repository;

namespace VKStats.SqlRepository
{
    public class SqlRepositoryFactory : IRepositoryFactory
    {
        /// <summary>
        /// Mapping between shard key and database.
        /// </summary>
        private Dictionary<byte, byte> shardMap = new Dictionary<byte, byte>
        {
            [0x00] = 0,
            [0x01] = 0,
            [0x02] = 0,
            [0x03] = 0,
            [0x04] = 0,
            [0x05] = 0,
            [0x06] = 0,
            [0x07] = 0,
            [0x08] = 15,
            [0x09] = 15,
            [0x0A] = 15,
            [0x0B] = 15,
            [0x0C] = 15,
            [0x0D] = 15,
            [0x0E] = 15,
            [0x0F] = 15,
        };

        byte GetShardNumber(int userId)
        {
            return shardMap[(byte) (userId % 0x10)];
        }

        public IPostRepository GetPostRepository(int userId)
        {
            var connectionStringTemplate = CloudConfigurationManager.GetSetting("PostDataConnectionString");
            if (string.IsNullOrEmpty(connectionStringTemplate))
                throw new Exception("PostDataConnectionString is not set.");

            var databaseName = string.Format("VKStatsData{0:D3}", GetShardNumber(userId));
            var context = new PostDataContext(string.Format(connectionStringTemplate, databaseName));

            return new PostRepository(context);
        }

        public IWallRepository GetWallRepository(int wallOwnerId)
        {
            var connectionStringTemplate = CloudConfigurationManager.GetSetting("PostDataConnectionString");
            if (string.IsNullOrEmpty(connectionStringTemplate))
                throw new Exception("PostDataConnectionString is not set.");

            // Use one database for all walls.
            var connectionString = string.Format(connectionStringTemplate, "VKStatsWeb_db");
            var context = new WallDataContext(connectionString);

            return new WallRepository(context);
        }
    }
}
