﻿using System.Collections.Generic;
using System.Linq;
using VKStats.Domain.Entity;
using VKStats.Domain.Repository;

namespace VKStats.SqlRepository
{
    internal class WallRepository : IWallRepository
    {
        private readonly WallDataContext dbContext;

        public WallRepository(WallDataContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public int AddRecord(WallRecord record)
        {
            dbContext.WallRecords.Add(record);
            dbContext.SaveChanges();

            return record.Id;
        }

        public IEnumerable<WallRecord> GetTopRecords(int wallOwnerId, int startFrom, int count)
        {
            if (startFrom == 0)
            {
                return new List<WallRecord>();
            }

            var query = dbContext.WallRecords.Where(wr => wr.WallOwnerId == wallOwnerId);

            if (startFrom > 0)
            {
                query = query.Where(wr => wr.Id <= startFrom);
            }

            return query.OrderByDescending(wr => wr.Id).Take(count);
        }

        public int GetTopId(int wallOwnerId)
        {
            var row = GetTopRecords(wallOwnerId, -1, 1).FirstOrDefault();
            return row != null ? row.Id : 0;
        }
    }
}