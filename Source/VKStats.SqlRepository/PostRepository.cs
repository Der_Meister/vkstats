﻿using System.Collections.Generic;
using System.Linq;
using VKStats.Domain.Entity;
using VKStats.Domain.Repository;

namespace VKStats.SqlRepository
{
    public class PostRepository : IPostRepository
    {
        private readonly PostDataContext dbContext;

        public PostRepository(PostDataContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public void AddOrUpdatePost(Post post)
        {
            var existingPost = dbContext.Posts.FirstOrDefault(p => p.PostId == post.PostId && p.UserId == post.UserId);

            if (existingPost == null)
            {
                dbContext.Posts.Add(post);
            }
            else
            {
                existingPost.Likes = post.Likes;
                existingPost.Length = post.Length;
            }
            dbContext.SaveChanges();
        }

        public void AddPosts(ICollection<Post> posts)
        {
            foreach (var post in posts)
            {
                dbContext.Posts.Add(post);
            }
            dbContext.SaveChanges();
        }

        public IEnumerable<Post> GetUserPosts(int userId)
        {
            return dbContext.Posts.Where(p => p.UserId == userId).ToList();
        }
    }
}